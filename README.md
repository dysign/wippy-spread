# WIPPY for Spread #

By @maximebj for wp-spread.com

### Automatize WordPress installs with WP-CLI ###

Wippy allows you to quickly setup a WordPress install in one click. 
Version 1.0 12/2014

### How to ###

Follow instructions on http://wp-spread.com/tuto-wp-cli-comment-installer-et-configurer-wordpress-en-moins-dune-minute-et-en-seulement-un-clic

